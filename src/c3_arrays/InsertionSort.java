package c3_arrays;

/**
 * Created by Keerthikan on 07-Feb-16.
 */
public class InsertionSort {

    public static void sort(char[] data){
        int n = data.length;
        for(int k = 1; k < n; k++){
            char c = data[k];
            int j = k;
            while(j > 0 && data[j-1] > c){
                data[j] = data[j-1];
                j--;
            }
            data[j] = c;
        }
    }

    public static void main(String[] arg){
        char[] data = {'b','c','d','a','g','t','s','u','f'};
        print(data);
        sort(data);
        print(data);
    }

    public static void print(char[] array){
        for(char t : array){
            System.out.print(t + " ");
        }
        System.out.println();
    }
}
